#ifndef _HOOK_H
#define _HOOK_H

// includes for this header
#include <ntifs.h>
#include <ntddk.h>
#include "structures.h"
#include "memory.h"

// stores an handle to our current process
HANDLE pid;

/**
 * The function `HookKrnlFunct` is used to hook a kernel function by replacing it with a detour
 * function.
 * 
 * Args:
 *   detour (void): The "detour" parameter is a pointer to the function that you want to use as the
 * detour or replacement for the original function.
 *   original (PVOID): A pointer to the original function that you want to detour.
 * 
 * Returns:
 *   a boolean value, either TRUE or FALSE.
 */

BOOLEAN HookKrnlFunct(void* detour, PVOID* original)
{
	// verify that the original function address is != null
	if (!original) return FALSE;

	// store the detoured address as an unsigned long long
	UINT_PTR detour_addr = (UINT_PTR)detour;

	// verify that the function detour is != null
	if (!detour_addr) return FALSE;

	// shell for trampoline hook
	BYTE trampoline[] =
	{
		//0x48, 0x89, 0x5c, 0x24, 0x10, //  mov [], rbx
		//0x48, 0x89, 0x74, 0x24, 0x18, //  mov [], rsi
		0x90, // nop
		0x90, // nop 
		0x90, // nop
		0x48, 0xB8, // mov rax, 
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, // [xxx]
		//0x48, 0x81, 0xec, 0x40, 0x01, 0x00, 0x00, // sub rsp, 140h
		0x90, // nop
		0x48, 0xB8, // mov rax, 
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, // [xxx]
		//0x48, 0x8B, 0xF1, // mov rsi, rcx
		0xFF, 0xE0, // jmp rax // d0 for call
		0xCC, // int3
	};

	// write the trampoline to the detour
	memcpy(trampoline + 16, &detour_addr, sizeof(detour_addr));

	// allocate a memory descripter list
	PMDL mdl = IoAllocateMdl(original, sizeof(trampoline), FALSE, FALSE, NULL);

	// check that memory descriptor list is != null
	if (!mdl) return FALSE;

	// make the mdl a resident and lock it in memory
	MmProbeAndLockPages(mdl, KernelMode, IoReadAccess);

	// routine map the physical pages
	PVOID routine_map = MmMapLockedPagesSpecifyCache(mdl, KernelMode, MmNonCached, NULL, FALSE, NormalPagePriority);

	// give write perm to the memory descriptor list
	MmProtectMdlSystemAddress(mdl, PAGE_READWRITE);

	if (!RtlCopyMemory(routine_map, &trampoline, sizeof(trampoline))) return FALSE;

	// unmap the routine map
	MmUnmapLockedPages(routine_map, mdl);

	// unlock the physical pages
	MmUnlockPages(mdl);

	// free the memory descriptor list
	IoFreeMdl(mdl);

	// function hook was successful
	return TRUE;
}

/**
 * 
 * Args:
 *   called_param (PVOID): The parameter `called_param` is a pointer to a structure of type
 * `memory_struct`.
 * 
 * Returns:
 *   the status code `STATUS_SUCCESS`.
 */
NTSTATUS HK_NtDxgkGetTrackedWorkloadStatistics(PVOID called_param)
{
	memory_struct* m = (memory_struct*)called_param;

	if (m->get_pid != FALSE)
		GetPID(&pid, m->get_pid_kernel.process_name);
	else if (m->get_base != FALSE)
	{
		ANSI_STRING AS;
		UNICODE_STRING ModuleName;

		RtlInitAnsiString(&AS, m->get_base_kernel.module_name);
		RtlAnsiStringToUnicodeString(&ModuleName, &AS, TRUE);	

		PEPROCESS process;
		PsLookupProcessByProcessId((HANDLE)pid, &process);
		UINT64 base_addy = GetMBA64(process, ModuleName);

		m->get_base_kernel.base_address = base_addy;
		RtlFreeUnicodeString(&ModuleName);
	}
	else if (m->write != FALSE)
	{
		PVOID kernelBuff = ExAllocatePool(NonPagedPool, m->write_kernel.size);

		if (!kernelBuff)
			return STATUS_UNSUCCESSFUL;

		if (!memcpy(kernelBuff, m->write_kernel.buffer_address, m->write_kernel.size))
			return STATUS_UNSUCCESSFUL;

		PEPROCESS Process;
		PsLookupProcessByProcessId((HANDLE)pid, &Process);
		WriteKrnlMemory((HANDLE)pid, m->write_kernel.address, kernelBuff, m->write_kernel.size);
		ExFreePool(kernelBuff);
	}
	else if (m->read != FALSE)
	{
		ReadKrnlMemory((HANDLE)pid, m->read_kernel.address, m->read_kernel.output, m->read_kernel.size);
	}
	else if (m->scan_for_klass != FALSE)
	{
		ANSI_STRING AS;
		PEPROCESS process;
		UNICODE_STRING GameAssemblyName;

		/* Convert "GameAssembly.dll" to UNICODE_STRING */
		RtlInitAnsiString(&AS, "GameAssembly.dll");
		RtlAnsiStringToUnicodeString(&GameAssemblyName, &AS, TRUE);

		/* process handle */
		PsLookupProcessByProcessId((HANDLE)pid, &process);

		/* get GameAssembly.dll addy */
		ULONG64 base = GetMBA64(process, GameAssemblyName);

		/* scan for klass */
		ULONG64 klass = ScanForClass(m->scan_class_kernel.class_name, base, pid);

		/* set output */
		m->scan_class_kernel.klass = klass;

		RtlFreeUnicodeString(&GameAssemblyName);
	}

	return STATUS_SUCCESS;
}

#endif // _HOOK_H