// Includes for this file
#include "mmunloaded.h"

/**
 * The function `bDataCompare` compares two arrays of bytes using a mask and returns true if they
 * match.
 * 
 * Args:
 *   pData (BYTE): pData is a pointer to an array of bytes that represents the data to be compared.
 *   bMask (BYTE): The bMask parameter is a pointer to an array of bytes. It is used to compare against
 * the data pointed to by pData.
 *   szMask (char): The szMask parameter is a string that represents a mask for comparing data. It
 * consists of characters that can be either 'x' or any other character. The 'x' character represents a
 * wildcard, meaning that the corresponding byte in the data can have any value. Any other character in
 * the mask represents
 * 
 * Returns:
 *   a boolean value. It returns 0 (false) if there is a mismatch between the data and the mask, and it
 * returns 1 (true) if there is a match or if the mask has been fully processed.
 */
BOOLEAN bDataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask)
{
	for (; *szMask; ++szMask, ++pData, ++bMask)
		if (*szMask == 'x' && *pData != *bMask)
			return 0;

	return (*szMask) == 0;
}

/**
 * The function PatternScan searches for a specific pattern of bytes within a given memory range and
 * returns the address where the pattern is found.
 * 
 * Args:
 *   dwAddress (UINT64): The starting address of the memory region to scan for the pattern.
 *   dwLen (UINT64): The dwLen parameter represents the length of the memory region to scan for a
 * specific pattern.
 *   bMask (BYTE): The bMask parameter is a pointer to an array of bytes that represents the pattern to
 * search for. Each byte in the array corresponds to a byte in memory that will be compared during the
 * search.
 *   szMask (char): The szMask parameter is a string that represents the pattern to search for. It
 * consists of characters that specify the bytes to compare against. Each character in the szMask
 * string corresponds to a byte in the bMask array. The characters in szMask can be 'x' or '?' to
 * indicate a wildcard
 * 
 * Returns:
 *   a UINT64 value, which is the address where the pattern is found. If the pattern is not found, the
 * function returns 0.
 */
UINT64 PatternScan(UINT64 dwAddress, UINT64 dwLen, BYTE* bMask, char* szMask)
{
	for (UINT64 i = 0; i < dwLen; i++)
		if (bDataCompare((BYTE*)(dwAddress + i), bMask, szMask))
			return (UINT64)(dwAddress + i);

	return 0;
}



/**
 * The function ClearMmUnloaded clears the MmUnloadedDrivers array in memory.
 * 
 * Returns:
 *   a boolean value. It returns TRUE if the Mm unloaded drivers are successfully cleared, and FALSE
 * otherwise.
 */
BOOLEAN ClearMmUnloaded()
{
	// create a buffer to store the system module information
	ULONG bytes = 0;

	// retrieve the system module information
	NTSTATUS status = ZwQuerySystemInformation(SystemModuleInformation, 0, bytes, &bytes);

	// check that the system module information was put into the buffer
	if (!bytes)
	{
		// log the error
		Log("(1) failed status 0x%x", status);

		return FALSE;
	}

	// retrieve the modules loaded
	PRTL_PROCESS_MODULES modules = (PRTL_PROCESS_MODULES)ExAllocatePoolWithTag(NonPagedPool, bytes, 0x454E4F45);

	// get the module system module information
	status = ZwQuerySystemInformation(SystemModuleInformation, modules, bytes, &bytes);

	// check that all instructions so far are NT_SUCCESS
	if (!NT_SUCCESS(status))
	{
		// log the error
		Log("(2) failed status 0x%x", status);

		return FALSE;
	}

	// retrieve the modules from the module
	PRTL_PROCESS_MODULE_INFORMATION module = modules->Modules;

	// create varibles to save the ntoskrnl base and size
	UINT64 ntoskrnl_base = 0, ntoskrnl_size = 0;

	// traverse the number modules from process
	for (ULONG i = 0; i < modules->NumberOfModules; i++)
	{
		// log this module
		//Log("CleanUnloadedDrivers: %s", module[i].FullPathName);

		// compare this it's module against ntoskrnl
		if (!strcmp((char*)module[i].FullPathName, "\\SystemRoot\\system32\\ntoskrnl.exe"))
		{
			// store ntoskrnl's base address 
			ntoskrnl_base = (UINT64)module[i].ImageBase;

			// store ntoskrnl's size
			ntoskrnl_size = (UINT64)module[i].ImageSize;

			break;
		}
	}

	// check if the modules were found
	if (modules) ExFreePoolWithTag(modules, 0);

	// check if the ntoskrnl is != null. (( Check if it's less than 0 because it is possible ))
	if (ntoskrnl_base <= 0)
	{
		// log that ntoskrnl is null 
		Log("ntoskrnl == NULL");

		return FALSE;
	}

	// pattern scan for a pointer to Mm unloaded drivers
	UINT64 mmunloaded_drivers_ptr = PatternScan((UINT64)ntoskrnl_base, (UINT64)ntoskrnl_size, (BYTE*)"\x4C\x8B\x00\x00\x00\x00\x00\x4C\x8B\xC9\x4D\x85\x00\x74", "xx?????xxxxx?x");

	// check that the pointer to Mm unloaded drivers is != NULL
	if (!mmunloaded_drivers_ptr)
	{
		Log("Mmunloaded_drivers_ptr == NULL");

		return FALSE;
	}

	// get the address to Mm unloaded drivers
	UINT64 Mmunloaded_drivers = (UINT64)((PUCHAR)mmunloaded_drivers_ptr + *(PULONG)((PUCHAR)mmunloaded_drivers_ptr + 3) + 7);

	// store Mm unloaded drivers in a buffer
	UINT64 bufferPtr = *(UINT64*)Mmunloaded_drivers;

	// NOTE: 0x7D0 is the size of the MmUnloadedDrivers array for win 7 and above
	PVOID new_buffer = ExAllocatePoolWithTag(NonPagedPoolNx, 0x7D0, 0x54446D4D);

	// check that the new buffer is not == NULL
	if (!new_buffer)
	{
		// log that the buffer is empty
		Log("buffer is empty");

		return FALSE;
	}

	// set the buffer to 0
	memset(new_buffer, 0, 0x7D0);

	// replace Mm unloaded drivers
	*(UINT64*)Mmunloaded_drivers = (UINT64)new_buffer;

	// clear the old buffer
	ExFreePoolWithTag((PVOID)bufferPtr, 0x54446D4D);

	return TRUE;
}