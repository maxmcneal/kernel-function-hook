#ifndef _MEMORY_HPP
#define _MEMORY_HPP

// includes for this header file
#include "structures.h"

/**
 * The function `GetSystemModuleBaseAddr` retrieves the base address of a specified system module in C.
 * 
 * Args:
 *   module_ (char): The parameter "module_" is a pointer to a character array that represents the name
 * of the module for which we want to retrieve the base address.
 * 
 * Returns:
 *   the base address of the specified system module.
 */
UINT64 GetSystemModuleBaseAddr(const char* module_)
{
    // Allocate a buffer 
    ULONG bytes = NULL;

    // Query the system information
    NTSTATUS status = ZwQuerySystemInformation(SystemModuleInformation, 0, bytes, &bytes);

    // Check that the buffer isn't empty
    if (!bytes) return NULL;

    // Retrieve the modules loaded
    PRTL_PROCESS_MODULES modules = (PRTL_PROCESS_MODULES)ExAllocatePoolWithTag(NonPagedPool, bytes, 0x454E4F45);

    // Get the module system module information
    status = ZwQuerySystemInformation(SystemModuleInformation, modules, bytes, &bytes);

    // Check that all instructions so far are NT_SUCCESS
    if (!NT_SUCCESS(status)) return NULL;

    // Retrieve the modules from the module
    PRTL_PROCESS_MODULE_INFORMATION module = modules->Modules;

    // Create varibles to save the ntoskrnl base and size
    UINT64 module_base = 0;

    // Traverse the number modules from process
    for (ULONG i = 0; i < modules->NumberOfModules; i++)
    {
        // Compare this it's module against ntoskrnl
        if (!strcmp((char*)module[i].FullPathName, module_))
        {
            // Store ntoskrnl's base address 
            module_base = (UINT64)module[i].ImageBase;

            break;
        }
    }

    // Check if the modules were found
    if (modules) ExFreePoolWithTag(modules, 0);

    // Check if the ntoskrnl is != null. (( Check if it's less than 0 because it is possible ))
    if (module_base <= 0) return NULL;

    return module_base;
}

/**
 * The function retrieves the export address of a specified module in the system.
 * 
 * Args:
 *   _module (char): The _module parameter is a string that represents the name of the module (DLL)
 * from which you want to retrieve the export address.
 *   _export (LPCSTR): The `_export` parameter is a LPCSTR (Long Pointer to a Constant String) that
 * represents the name of the export symbol (function or variable) that you want to retrieve from the
 * specified module.
 * 
 * Returns:
 *   a pointer to the export function with the given name in the specified module.
 */
PVOID GetSystemModuleExport(const char* _module, LPCSTR _export)
{
    // Retrieve the module base address
    PVOID module_address = (PVOID)GetSystemModuleBaseAddr(_module);

    // Check that the module base address is != null
    if (!module_address) return NULL;

    // Get the export address
    return RtlFindExportedRoutineByName(module_address, _export);
}

/**
 * The function `GetPID` retrieves the process ID (PID) of a specified process name in Windows.
 * 
 * Args:
 *   pid (HANDLE): A pointer to a HANDLE variable where the PID (Process ID) will be stored.
 *   process (char): The `process` parameter is a pointer to a null-terminated string that represents
 * the name of the process for which you want to retrieve the process ID (PID).
 * 
 * Returns:
 *   an NTSTATUS value, which indicates the success or failure of the function.
 */
NTSTATUS GetPID(HANDLE* pid, const char* process)
{
	// ZwQuery
	ULONG CBLength = 0;
	PVOID bPid = NULL;
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	PSYSTEM_PROCESS_INFO PSI = NULL;
	PSYSTEM_PROCESS_INFO pCurrent = NULL;

	ANSI_STRING AS;
	RtlInitAnsiString(&AS, process);

	// convert string to unicode
	UNICODE_STRING processName;
	RtlAnsiStringToUnicodeString(&processName, &AS, TRUE);

	// prints status 

	// need to loop because new process spawn making our buffer already to small
here:;
	if (!NT_SUCCESS(ZwQuerySystemInformation(SystemProcessInformation, NULL, NULL, &CBLength)))
	{
		bPid = ExAllocatePoolWithTag(NonPagedPool, CBLength, 0x616b7963);
		if (!bPid)
		{
			return Status;
		}

		PSI = (PSYSTEM_PROCESS_INFO)bPid;
		Status = ZwQuerySystemInformation(SystemProcessInformation, PSI, CBLength, NULL);
		if (!NT_SUCCESS(Status))
		{
			ExFreePoolWithTag(bPid, 0x616b7963);
			goto here;
			return Status = STATUS_INFO_LENGTH_MISMATCH;
		}

		// print status

		do
		{
			if (PSI->NextEntryOffset == 0)
				break;

			if (RtlEqualUnicodeString(&processName, &PSI->ImageName, FALSE))
			{
				DbgPrintEx(0, 0, "[skrrt.driver] PID %d | NAME %ws\n", PSI->UniqueProcessId, PSI->ImageName.Buffer);
				*pid = PSI->UniqueProcessId;
				Status = STATUS_SUCCESS;
				break;
			}

			// calculate the address of the next entry
			PSI = (PSYSTEM_PROCESS_INFO)((unsigned char*)PSI + PSI->NextEntryOffset);

		} while (PSI->NextEntryOffset);

		// free allocated memory
		ExFreePoolWithTag(bPid, 0x616b7963);
	}

	// return status 
	return Status;
}

/**
 * The function GetMBA64 retrieves the base address of a module in a given process.
 * 
 * Args:
 *   proc (PEPROCESS): The "proc" parameter is of type PEPROCESS and represents a pointer to the
 * process object of the target process.
 *   module_name (UNICODE_STRING): The module_name parameter is a UNICODE_STRING that represents the
 * name of the module (DLL) that you want to find the base address of.
 * 
 * Returns:
 *   a UINT64 value, which represents the base address of a module in the specified process. If the
 * module is found, the base address is returned. If the module is not found or if there is an error,
 * the function returns 0.
 */
UINT64 GetMBA64(PEPROCESS proc, UNICODE_STRING module_name)
{
	PPEB pPeb = (PPEB)PsGetProcessPeb(proc);

	if (!pPeb)
		return 0;

	KAPC_STATE state;

	KeStackAttachProcess(proc, &state);

	PPEB_LDR_DATA pLdr = (PPEB_LDR_DATA)pPeb->Ldr;

	if (!pLdr) {
		KeUnstackDetachProcess(&state);
		return 0;
	}

	for (PLIST_ENTRY list = (PLIST_ENTRY)pLdr->ModuleListLoadOrder.Flink;
		list != &pLdr->ModuleListLoadOrder; list = (PLIST_ENTRY)list->Flink) {
		PLDR_DATA_TABLE_ENTRY pEntry =
			CONTAINING_RECORD(list, LDR_DATA_TABLE_ENTRY, InLoadOrderModuleList);
		if (RtlCompareUnicodeString(&pEntry->BaseDllName, &module_name, TRUE) ==
			0) {
			UINT64 baseAddr = (UINT64)pEntry->DllBase;
			KeUnstackDetachProcess(&state);
			return baseAddr;
		}
	}

	KeUnstackDetachProcess(&state);
	return 0;
}

/**
 * The function `WriteKrnlMemory` allows writing data to a specified memory address in a given process.
 * 
 * Args:
 *   pid (HANDLE): The process ID of the target process.
 *   address (uintptr_t): The address parameter is the starting address in the target process where the
 * data will be written to.
 *   buffer (void): The buffer parameter is a pointer to the data that you want to write to the kernel
 * memory.
 *   size (SIZE_T): The size parameter represents the size of the buffer in bytes. It indicates the
 * amount of memory that needs to be written to the specified address.
 * 
 * Returns:
 *   a BOOLEAN value.
 */
BOOLEAN WriteKrnlMemory(HANDLE pid, uintptr_t address, void* buffer, SIZE_T size)
{
	if (!address || !buffer || !size)
		return FALSE;

	NTSTATUS Status = STATUS_SUCCESS;
	PEPROCESS process;
	PsLookupProcessByProcessId(pid, &process);

	KAPC_STATE state;
	KeStackAttachProcess((PKPROCESS)process, &state);

	__try
	{
		MEMORY_BASIC_INFORMATION info;

		Status = ZwQueryVirtualMemory(ZwCurrentProcess(), (PVOID)address, MemoryBasicInformation, &info, sizeof(info), NULL);
		if (!NT_SUCCESS(Status)) {
			KeUnstackDetachProcess(&state);
			return FALSE;
		}

		if (((uintptr_t)info.BaseAddress + info.RegionSize) < (address + size))
		{
			KeUnstackDetachProcess(&state);
			return FALSE;
		}

		if (!(info.State & MEM_COMMIT) || (info.Protect & (PAGE_GUARD | PAGE_NOACCESS)))
		{
			KeUnstackDetachProcess(&state);
			return FALSE;
		}

		if ((info.Protect & PAGE_EXECUTE_READWRITE) || (info.Protect & PAGE_EXECUTE_WRITECOPY) || (info.Protect & PAGE_READWRITE) || (info.Protect & PAGE_WRITECOPY))
		{
			RtlCopyMemory((void*)address, buffer, size);
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		Log("caught exception when writing..");
	}

	KeUnstackDetachProcess(&state);
	ObDereferenceObject(process);
	return TRUE;
}

/**
 * The function ReadKrnlMemory reads memory from a specified process in kernel mode and returns a
 * boolean value indicating success or failure.
 * 
 * Args:
 *   pid (HANDLE): The process ID of the target process from which you want to read memory.
 *   address (uintptr_t): The address in kernel memory that you want to read from.
 *   buffer (void): The buffer is a pointer to the memory location where the read data will be stored.
 *   size (SIZE_T): The "size" parameter is the number of bytes to read from the kernel memory starting
 * at the specified address.
 * 
 * Returns:
 *   a BOOLEAN value.
 */
BOOLEAN ReadKrnlMemory(HANDLE pid, uintptr_t address, void* buffer, SIZE_T size)
{
	if (!address || !buffer || !size)
		return FALSE;

	SIZE_T bytes = 0;
	NTSTATUS status = STATUS_SUCCESS;
	PEPROCESS process;
	PsLookupProcessByProcessId((HANDLE)pid, &process);

	__try
	{
		if (address < 0x7FFFFFFFFFFF)
		{
			status = MmCopyVirtualMemory(process, (void*)address, (PEPROCESS)PsGetCurrentProcess(), buffer, size, KernelMode, &bytes);
			if (!NT_SUCCESS(status))
			{
				//Log("read-failed, bytes copied: %x\n", bytes);
				return FALSE;
			}
			else
			{
				//Log("read-sucessful, bytes copied: %x\n", bytes);

				ObDereferenceObject(process);
				return TRUE;
			}
		}
		else
		{
			ObDereferenceObject(process);
			return FALSE;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		Log("caught exception while reading..");
	}

	ObDereferenceObject(process);
}

/**
 * The function `ScanForClass` scans the memory of a process to find a class with a given name.
 * 
 * Args:
 *   class_name (char): The class name you want to scan for in the memory.
 *   base (UINT64): The base address of the module in the target process's memory. This is the starting
 * address of the module's image in memory.
 *   pid (HANDLE): The `pid` parameter is the process ID of the target process.
 * 
 * Returns:
 *   The function `ScanForClass` returns a `UINT64` value.
 */
UINT64 ScanForClass(const char* class_name, UINT64 base, HANDLE pid)
{
	IMAGE_DOS_HEADER dos_header;
	ReadKrnlMemory((HANDLE)pid, base, &dos_header, sizeof(dos_header));

	IMAGE_SECTION_HEADER data_header;
	ReadKrnlMemory((HANDLE)pid, base + dos_header.e_lfanew + sizeof(IMAGE_NT_HEADERS64) + (3 * 40), &data_header, sizeof(data_header));

	IMAGE_SECTION_HEADER next_section;
	ReadKrnlMemory((HANDLE)pid, base + dos_header.e_lfanew + sizeof(IMAGE_NT_HEADERS64) + (4 * 40), &next_section, sizeof(next_section));

	DWORD data_size = next_section.VirtualAddress - data_header.VirtualAddress;

	// check if this section is .data, if not, return 0
	if (strcmp((char*)data_header.Name, ".data"))
	{
		return 0;
	}

	// scan for the requested class
	for (ULONG64 offset = data_size; offset > 0; offset -= 8)
	{
		ULONG64 klass;
		ReadKrnlMemory((HANDLE)pid, base + data_header.VirtualAddress + offset, &klass, sizeof(klass));
		if (klass == 0) { continue; }

		ULONG64 name_pointer;
		ReadKrnlMemory((HANDLE)pid, klass + 0x10, &name_pointer, sizeof(name_pointer));
		if (name_pointer == 0) { continue; }

		char klass_name[0x100] = { 0 };
		ReadKrnlMemory((HANDLE)pid, name_pointer, &klass_name, sizeof(klass_name));
		if (!strcmp(klass_name, class_name))
		{
			return klass;
		}
	}

	return 0;
}

#endif // _MEMORY_HPP