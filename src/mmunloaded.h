#ifndef _MMULOADED_HPP_
#define _MMULOADED_HPP_

// includes for this header
#include <ntifs.h>
#include <ntddk.h>
#include "structures.h"

BOOLEAN ClearMmUnloaded();

#endif // _MMULOADED_H_
