// includes
#include <ntifs.h>
#include "mmunloaded.h"
#include "hook.h"

//
// drivers's real entry point
//
int RealEntry()
{
	Log("real entry called");

	// clean mmUnloaded
	if (ClearMmUnloaded() == TRUE) Log("cleared MMUnloadedDrivers");

	// install hooks
	if (HookKrnlFunct(HK_NtDxgkGetTrackedWorkloadStatistics, (PVOID*)GetSystemModuleExport("\\SystemRoot\\System32\\drivers\\dxgkrnl.sys", "NtDxgkGetTrackedWorkloadStatistics")) == TRUE) Log("installed kernel function hook");

	// return success
	return STATUS_SUCCESS;
}

//
// driver's entry point
//
NTSTATUS DriverEntry(PVOID lpBaseAddress, DWORD32 dwSize)
{
	Log("driver created");

	// call real entry point
	RealEntry();

	// return success
	return STATUS_SUCCESS;
}