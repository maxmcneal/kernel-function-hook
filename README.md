<div align="center">
    <h1>
        <img src="images/header.png" alt="Kernel Function Hook">
    </h1>
    <h4>
        Inline function hook, exploiting an vulnerability in dxgkrnl.sys, intercepting function calls within the DirectX kernel driver, primarily facilitating low-level UM-KM communciation. 
    </h4>
</div>

<div align="center">
  <img src="https://img.shields.io/github/v/release/nhn/tui.editor.svg?include_prereleases">
  <img src="https://img.shields.io/github/license/nhn/tui.editor.svg">
  <img src="https://img.shields.io/badge/%3C%2F%3E%20with%20%E2%99%A5-ff1414.svg">
  <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat">
</div>

<div align="center">
  <a href="#-goals-and-requirements">Goals and Requirements</a> •
  <a href="#-key-learnings">Key Learnings</a> •
  <a href="#-installation">Installation</a> •
  <a href="#-contributing">Contributing</a> •
  <a href="#-credits">Credits</a> •
  <a href="#-license">License</a>
</div>

<br/>

![screenshot](images/banner.png)


## 🎯 Goals and Requirements
- Efficient interception and monitoring of function calls within dxgkrnl.sys
- Minimal system footprint
- PatchGuard compatibility and compliance
- Clear MmUnloadedDrivers list, to prevent traces of driver mapping

## 📙 Key learnings
- Understanding of windows kernel architecture
- Understanding of memory management, pointer arithmetic, and direct hardware access
- Reverse engineering and binary analysis using IDA Pro and Ghidra

## ⚙️ Installation
- TBA

## 🐛 Contributing
- TBA

## 💭 Credits
- TBA

## 📜 License

This project is licensed under the terms of the MIT license and protected by Udacity Honor Code and Community Code of Conduct. See <a href="LICENSE.md">license</a> and <a href="LICENSE.DISCLAIMER.md">disclaimer</a>.
